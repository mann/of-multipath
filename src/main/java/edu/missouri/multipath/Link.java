package edu.missouri.multipath;

public class Link {

    private Node src;
    private Node dst;
    private double capacity;
    private double availableBandwidth;
    
    public Link(Node src, Node dst, double capacity, double availableBandwidth) {
        this.src = src;
        this.dst = dst;
        this.capacity = capacity;
        this.availableBandwidth = availableBandwidth;
    }

    public double getAvailableBandwidth() {
        return availableBandwidth;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        sb.append(src.getId() + "->" + dst.getId());
        
        return sb.toString();
    }
}
