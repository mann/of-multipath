package edu.missouri.multipath;

import java.util.HashMap;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;

public class Main {

    public static void main(String[] args) {
        Node h1 = new Node("h1", NodeType.HOST);
        Node h2 = new Node("h2", NodeType.HOST);
        Node s1 = new Node("s1", NodeType.SWITCH);
        Node s2 = new Node("s2", NodeType.SWITCH);
        Node s3 = new Node("s3", NodeType.SWITCH);
        Node s4 = new Node("s4", NodeType.SWITCH);
        
        Path path1 = new Path();
        path1.addLink(new Link(h1, s1, 100, 100));
        path1.addLink(new Link(s1, s2, 10, 10));
        path1.addLink(new Link(s2, s4, 10, 10));
        path1.addLink(new Link(s4, h2, 10, 10));
        
        Path path2 = new Path();
        path2.addLink(new Link(h1, s1, 100, 100));
        path2.addLink(new Link(s1, s3, 10, 10));
        path2.addLink(new Link(s3, s4, 10, 8));
        path2.addLink(new Link(s4, h2, 10, 10));
        
        Map<Path, Double> pathAndCost = new HashMap<Path, Double>();
        pathAndCost.put(path1, path1.getMaxBandwidth());
        pathAndCost.put(path2, path2.getMaxBandwidth());
        
        Topology topo = new Topology(pathAndCost);
        VLRequest req1 = new VLRequest("h1", 5161, "h2", 22);
        VLRequest req2 = new VLRequest("h1", 5162, "h2", 22);
        VLRequest req3 = new VLRequest("h1", 5163, "h2", 22);
        
//        topo.multipath(req1);
//        topo.multipath(req2);
        Map<Path, Set<VLRequest>> result = topo.multipath(req1);
        
        StringBuilder sb = new StringBuilder();
        sb.append("Path and Bandwidth:\n")
            .append(path1).append(": ")
            .append(path1.getMaxBandwidth()).append(" Mbps\n")
            .append(path2).append(": ")
            .append(path2.getMaxBandwidth()).append(" Mbps\n")
            .append("\n");
        sb.append("Requests:\n");
        
        int counter = 0;
        for (Entry<Path, Set<VLRequest>> entry : result.entrySet()) {
            Path path = entry.getKey();
            double maxBandwidth = path.getMaxBandwidth();
            int totalRequests = entry.getValue().size();
            if (totalRequests > 0) {
                int balanceBandwidth = (int)maxBandwidth / totalRequests;
                
                for (int i=0; i<totalRequests; i++) {
                    counter++;
                    sb.append("Request_").append(counter)
                        .append(" [").append(balanceBandwidth)
                        .append(" Mbps]: ")
                        .append(path.toString())
                        .append("\n");
                }
            }
        }
        System.out.println(sb.toString());
    }

}
