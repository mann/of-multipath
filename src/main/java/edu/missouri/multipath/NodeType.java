package edu.missouri.multipath;

public enum NodeType {
    HOST,
    SWITCH
}
