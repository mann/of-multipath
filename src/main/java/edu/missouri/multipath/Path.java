package edu.missouri.multipath;

import java.util.ArrayList;
import java.util.List;

public class Path implements Comparable<Path> {

    private List<Link> route;
    private double priority;
    
    public Path() {
        this.route = new ArrayList<Link>();
    }

    public void addLink(Link link) {
        route.add(link);
    }

    public double getMaxBandwidth() {
        double result = Double.MAX_VALUE;
        for (Link link : route) {
            double bandwidth = link.getAvailableBandwidth();
            if (result > bandwidth) {
                result = bandwidth;
            }
        }
        
        return result;
    }
    
    public void setPriority(double priority) {
        this.priority = priority;
    }
    
    public double getPriority() {
        return priority;
    }

    public int compareTo(Path other) {
        if (priority > other.getPriority()) {
            return -1;
        } else if (priority < other.getPriority()){
            return 1;
        }
        
        return 0;
    }
    
    @Override
    public String toString() {
        StringBuilder sb = new StringBuilder();
        for (Link link : route) {
            sb.append(link.toString())
                .append(", ");
        }
        sb.setLength(sb.length() - 2);
        
        return sb.toString();
    }
}
