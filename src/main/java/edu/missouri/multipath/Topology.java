package edu.missouri.multipath;

import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Map;
import java.util.Map.Entry;
import java.util.PriorityQueue;
import java.util.Queue;
import java.util.Set;

public class Topology implements Comparator<Path> {

    private Queue<Path> paths = new PriorityQueue<Path>();
    private Map<Path, Double> pathAndCost;
    
    private Map<Path, Set<VLRequest>> connections =
            new HashMap<Path, Set<VLRequest>>();
    
    public Topology(Map<Path, Double> pathAndCost) {
        this.pathAndCost = pathAndCost;
        for(Entry<Path, Double> entry : pathAndCost.entrySet()) {
            Path path = entry.getKey();
            path.setPriority(entry.getValue());
            paths.add(path);
            connections.put(path, new HashSet<VLRequest>());
        }
    }

    public void addPath(Path path) {
        paths.add(path);
    }

    public Map<Path, Set<VLRequest>> multipath(VLRequest reqeust) {
//        Map<Path, Set<VLRequest>> result = new HashMap<Path, Set<VLRequest>>();
        Path resultPath = paths.poll();
        Set<VLRequest> pathConnections = connections.get(resultPath);
        
        resultPath.setPriority(resultPath.getPriority() / (pathConnections.size() + 2));
        pathConnections.add(reqeust);
//        result.put(resultPath, pathConnections);
        paths.add(resultPath);
        
        return connections;
    }

    public int compare(Path path1, Path path2) {
        return path1.compareTo(path2);
    }
    
}
