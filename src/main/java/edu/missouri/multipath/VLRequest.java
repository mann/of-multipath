package edu.missouri.multipath;

import java.util.ArrayList;
import java.util.List;

public class VLRequest {

    private String srcIp;
    private String dstIp;
    private int srcPort;
    private int dstPort;
    
    private List<IVLConstraint> constraints =
            new ArrayList<IVLConstraint>();
    
    public VLRequest(String srcIp, int srcPort, String dstIp, int dstPort) {
        this.srcIp = srcIp;
        this.srcPort = srcPort;
        this.dstIp = dstIp;
        this.dstPort = dstPort;
    }

    public String getSrcIp() {
        return this.srcIp;
    }
    
    public String getDstIp() {
        return this.dstIp;
    }

    public void addConstraint(IVLConstraint constraint) {
        constraints.add(constraint);
    }

    public List<IVLConstraint> getConstraints() {
        return constraints;
    }

    public int getSrcPort() {
        return srcPort;
    }

    public void setSrcPort(short srcPort) {
        this.srcPort = srcPort;
    }

    public int getDstPort() {
        return dstPort;
    }

    public void setDstPort(short dstPort) {
        this.dstPort = dstPort;
    }

}
