package edu.missouri.multipath;

import static org.junit.Assert.assertEquals;

import org.junit.Test;

public class NodeTest {

    @Test
    public void should_get_id() {
        Node h1 = new Node("h1", NodeType.HOST);
        assertEquals("h1", h1.getId());
    }
}
