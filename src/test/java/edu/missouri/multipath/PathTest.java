package edu.missouri.multipath;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class PathTest {

    Path path;
    
    @Before
    public void setUp() {
        Node h1 = new Node("h1", NodeType.HOST);
        Node h2 = new Node("h2", NodeType.HOST);
        Node s1 = new Node("s1", NodeType.SWITCH);
        Link l1 = new Link(h1, s1, 100, 100);
        Link l2 = new Link(s1, h2, 100, 10);
        
        path = new Path();
        path.addLink(l1);
        path.addLink(l2);
    }
    
    @Test
    public void should_getBandwidth() {
        assertEquals(10, path.getMaxBandwidth(), 0.1);
    }
}
