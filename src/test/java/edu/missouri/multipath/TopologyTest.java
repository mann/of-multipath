package edu.missouri.multipath;

import static org.junit.Assert.assertEquals;

import java.util.HashMap;
import java.util.Map;
import java.util.Set;

import org.junit.Before;
import org.junit.Test;

public class TopologyTest {

    Node h1;
    Node h2;
    
    Node s1;
    Node s2;
    Node s3;
    Node s4;
    
    Path path1;
    Path path2;
    
    Topology topo;
    VLRequest req1;
    VLRequest req2;
    
    @Before
    public void setUp() {
        h1 = new Node("h1", NodeType.HOST);
        h2 = new Node("h2", NodeType.HOST);
        s1 = new Node("s1", NodeType.SWITCH);
        s2 = new Node("s2", NodeType.SWITCH);
        s3 = new Node("s3", NodeType.SWITCH);
        s4 = new Node("s4", NodeType.SWITCH);
        
        path1 = new Path();
        path1.addLink(new Link(h1, s1, 100, 100));
        path1.addLink(new Link(s1, s2, 10, 10));
        path1.addLink(new Link(s2, s4, 10, 10));
        path1.addLink(new Link(s4, h2, 10, 10));
        
        path2 = new Path();
        path2.addLink(new Link(h1, s1, 100, 100));
        path2.addLink(new Link(s1, s3, 10, 10));
        path2.addLink(new Link(s3, s4, 10, 8));
        path2.addLink(new Link(s4, h2, 10, 10));
        
        Map<Path, Double> pathAndCost = new HashMap<Path, Double>();
        pathAndCost.put(path1, path1.getMaxBandwidth());
        pathAndCost.put(path2, path2.getMaxBandwidth());
        
        topo = new Topology(pathAndCost);
        req1 = new VLRequest("h1", 5161, "h2", 22);
        req2 = new VLRequest("h1", 5162, "h2", 22);
    }
    
    @Test
    public void should_do_multipath()
    {
        Map<Path, Set<VLRequest>> paths;
        paths = topo.multipath(req1);
        Set<VLRequest> requests = paths.get(path1);
        assertEquals(1, paths.get(path1).size());
//        paths = topo.multipath(req2);
    }
}
