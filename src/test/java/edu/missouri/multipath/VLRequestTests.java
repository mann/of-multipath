package edu.missouri.multipath;

import static org.junit.Assert.assertEquals;

import org.junit.Before;
import org.junit.Test;

public class VLRequestTests {

    VLRequest request;
    String srcIp;
    String dstIp;
    
    @Before
    public void setUp() {
        srcIp = "10.0.0.1";
        dstIp = "10.0.0.2";
        request = new VLRequest(srcIp, 1, dstIp, 2);
    }
    
    @Test
    public void should_get_src_ip() {
        assertEquals(srcIp, request.getSrcIp());
    }
    
    @Test
    public void should_get_dst_ip() {
        assertEquals(dstIp, request.getDstIp());
    }
    
    @Test
    public void should_add_constrain() {
        request.addConstraint(new BandwidthConstraint());
        assertEquals(1, request.getConstraints().size());
    }
}
